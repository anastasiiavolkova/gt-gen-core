load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "0.9.14"

def mINI():
    maybe(
        http_archive,
        name = "mINI",
        sha256 = "e9cdba30e2c9f43d72dc3e3460507be439b5bcb35933e06f0969c775d4ff12ab",
        url = "https://github.com/pulzed/mINI/archive/refs/tags/{version}.tar.gz".format(version = _VERSION),
        build_file = Label("//:third_party/mINI/mINI.BUILD"),
        strip_prefix = "mINI-{version}/src/mini".format(version = _VERSION),
        add_prefix = "mINI",
        patch_args = ["-p1"],
        patches = [Label("//:third_party/mINI/comment_character.patch")],
    )

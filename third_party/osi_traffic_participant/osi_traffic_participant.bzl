load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v0.0.3"

def osi_traffic_participant():
    maybe(
        http_archive,
        name = "osi_traffic_participant",
        url =  "https://gitlab.eclipse.org/eclipse/openpass/osi-traffic-participant/-/archive/{tag}/osi-traffic-participant-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "e2ff0dda056fcccaaece4c5b069e693a00905895882559ac9db38186d6301653",
        strip_prefix = "osi-traffic-participant-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )

/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/environment_factory.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::api
{
using units::literals::operator""_ms;

TEST(EnvironmentFactoryTest, GivenUserSettings_WhenCreatingEnvironment_ThenGtGenEnvironmentCreated)
{
    auto environment = EnvironmentFactory::Create(service::user_settings::UserSettings{}, 40_ms);
    EXPECT_NE(environment, nullptr);
}

}  // namespace gtgen::core::environment::api

/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTERUTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTERUTILSS_H

#include "osi_object.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::environment::proto_groundtruth
{

using ProtoStationaryObjectClassification = osi3::StationaryObject::Classification;
ProtoStationaryObjectClassification::Type ConvertStaticObjectTypeToProtoStationaryObjectType(
    const mantle_api::StaticObjectType static_object_type);

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTERUTILS_H

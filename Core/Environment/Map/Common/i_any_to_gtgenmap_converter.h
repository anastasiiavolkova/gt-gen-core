/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOGTGENMAPCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOGTGENMAPCONVERTER_H

#include <MantleAPI/Common/i_identifiable.h>

#include <map>

namespace gtgen::core::environment::map
{
class GtGenMap;

class IAnyToGtGenMapConverter
{
  public:
    virtual ~IAnyToGtGenMapConverter() = default;

    virtual void Convert() = 0;

    virtual std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToGtGenTrafficLightIdMap() const = 0;

  protected:
    IAnyToGtGenMapConverter() = default;
    IAnyToGtGenMapConverter(const IAnyToGtGenMapConverter&) = default;
    IAnyToGtGenMapConverter(IAnyToGtGenMapConverter&&) = default;
    IAnyToGtGenMapConverter& operator=(const IAnyToGtGenMapConverter&) = default;
    IAnyToGtGenMapConverter& operator=(IAnyToGtGenMapConverter&&) = default;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOGTGENMAPCONVERTER_H

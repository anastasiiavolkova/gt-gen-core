/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_CONVERTFIRSTCUSTOMTRAFFICCOMMANDOFHOSTVEHICLETOROUTE_H
#define GTGEN_CORE_TESTS_TESTUTILS_CONVERTFIRSTCUSTOMTRAFFICCOMMANDOFHOSTVEHICLETOROUTE_H

#include "osi_trafficcommand.pb.h"

#include <cstdint>
#include <vector>

namespace gtgen::core::test_utils
{

std::vector<std::uint64_t> ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(
    const std::vector<osi3::TrafficCommand>& commands);

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_CONVERTFIRSTCUSTOMTRAFFICCOMMANDOFHOSTVEHICLETOROUTE_H

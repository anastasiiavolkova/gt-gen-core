/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"

#include <arpa/inet.h>
#include <gtest/gtest.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdint>

namespace gtgen::core::communication
{

class FoxgloveWebsocketServerFixture : public ::testing::Test
{
  protected:
    bool IsServingAtPort(std::uint16_t port)
    {
        auto client_socket = socket(AF_INET, SOCK_STREAM, 0);
        EXPECT_GT(client_socket, 0);

        struct sockaddr_in serv_addr;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(port);

        const std::string localhost{"127.0.0.1"};
        EXPECT_GT(inet_pton(AF_INET, localhost.c_str(), &serv_addr.sin_addr), 0);

        bool connected{connect(client_socket, reinterpret_cast<struct sockaddr*>(&serv_addr), sizeof(serv_addr)) == 0};
        close(client_socket);

        return connected;
    }

    FoxgloveWebsocketServer server_;
};

TEST_F(FoxgloveWebsocketServerFixture, GivenServerInstance_WhenNotStarted_ThenPort8765RefusesConnection)
{
    EXPECT_FALSE(IsServingAtPort(8765));
}

// Note: Test got disabled as an internal job fails due to this test
TEST_F(FoxgloveWebsocketServerFixture, DISABLED_GivenServerInstance_WhenStarted_ThenPort8765AcceptsConnection)
{
    server_.Start();
    EXPECT_TRUE(IsServingAtPort(8765));
}

}  // namespace gtgen::core::communication

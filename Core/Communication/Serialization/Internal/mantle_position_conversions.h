/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MANTLEPOSITIONCONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MANTLEPOSITIONCONVERSIONS_H

#include "editor_api.pb.h"
#include "gtgen_map.pb.h"
#include "scenario.pb.h"

#include <MantleAPI/Common/position.h>

namespace gtgen::core::communication
{

void FillLatLonProtoPosition(const mantle_api::Position& position, messages::scenario::LatLonPosition* proto_position);

void FillOdrRoadProtoPosition(const mantle_api::Position& position,
                              messages::scenario::OpenDrivePosition* proto_position);

void FillOdrLaneProtoPosition(const mantle_api::Position& position,
                              messages::scenario::OpenDrivePosition* proto_position);

void FillPosition(const mantle_api::Position& position, messages::scenario::GtGenPosition* proto_position);

void FillEditorResponsePosition(const mantle_api::Position& position, messages::editor::EditorApi* editor_api);

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MANTLEPOSITIONCONVERSIONS_H

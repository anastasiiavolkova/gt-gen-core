def _get_only_source_files_impl(ctx):
    gen_files_extensions = ["h", "cc"]
    return DefaultInfo(files = depset([f for f in ctx.files.srcs if f.extension in gen_files_extensions]))

get_only_source_files = rule(
    implementation = _get_only_source_files_impl,
    attrs = {
        "srcs": attr.label_list(allow_files = True, mandatory = True),
    },
)

# *******************************************************************************
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -ex

echo "Prepare ..."

# Create the CACHEDIR folder if it does not exist
mkdir -p /home/jenkins/cache/gtgen_core

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "/home/jenkins/cache/gtgen_core")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Wipe old artifactts if exist
rm -rf "${BASEDIR}/artifacts"

# Debug prints
echo "Path of MYDIR: ${MYDIR}"
echo "Path of BASEDIR: ${BASEDIR}"
echo "Path of CACHEDIR: ${CACHEDIR}"

# Bazel cache cleanup
# TODO: bazel 7.3.0 will support automatic cache cleanup, --experimental_disk_cache_max_size
# Once it is released, we should migrate bazel to 7.3.0, see details https://github.com/bazelbuild/bazel/issues/5139
cd "${CACHEDIR}"
folder_size=$(du -sh "${CACHEDIR}" | awk '{print $1}')
if ((${folder_size%G} > 50)); then
    echo "WARNING: Deleting Bazel cache CAS files older than 2 days..."
    # Find and delete files older than 2 days
    find "${CACHEDIR}/cas" -type f -mtime +2 -exec sh -c 'rm -f "{}"' \;
fi

# Debug prints
git --version
bazel --version
clang-tidy --version
buildifier --version

whereis git
whereis bazel
whereis clang-tidy
whereis buildifier

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

echo "Checkout git submodules recursively ..."
git submodule update --init --recursive
